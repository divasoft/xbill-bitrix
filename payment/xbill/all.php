<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
/**
 * Универсальный обработчик оплаты, если платежных систем на основе одного обработчика больше чем одна штука.
 * 
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 */

/*
 * Кусок кода для отладки входящих параметров
$req_dump = print_r($_SERVER, TRUE);
$fp = fopen('request.log', 'a');
fwrite($fp, $req_dump);
fclose($fp);*/

$psID = 5; // На всякий случай
$ptID = 1; // На всякий случай
$ordID = intval($_REQUEST['order']);

if ($ordID && CModule::IncludeModule('sale')) {
	$arOrder = CSaleOrder::GetByID($ordID);
	$psID = $arOrder['PAY_SYSTEM_ID'];
	$ptID = $arOrder['PERSON_TYPE_ID'];
}

$APPLICATION->IncludeComponent(
	'bitrix:sale.order.payment.receive',
	'',
	Array(
		'PAY_SYSTEM_ID' => $psID,
		'PERSON_TYPE_ID' => $ptID
	),
false
);
 require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>