<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
/**
 * Обработчик платежной системы, если в системе несколько обработчиков XBILL, рекомендуется использовать all.php
 * 
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 */
$APPLICATION->IncludeComponent('bitrix:sale.order.payment.receive','',
	Array(
		'PAY_SYSTEM_ID' => '5', // Укажите ID платежной системы
		'PERSON_TYPE_ID' => array('1') // Укажите ID типов плательщиков
	)
);
 require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>