<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); /**
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 * 
 * Библиотека "Мобильная комерция x-bill.org ( API v.1.2 )"
 * http://x-bill.org/download/mk_api.zip
 */?><?php

header("Content-Type: text/html; charset=UTF-8");

if(!function_exists("mk_get_phone")) {
    function mk_get_phone($phone) {
        return preg_replace('/[^0-9]/', '', $phone);
    }
}

# Инициализации платежа
if(!function_exists("mk_create_pay")) {
    function mk_create_pay() {
        global $mk_config;

        $send = array(
            "phone" =>$mk_config['phone'],
            "cost" =>$mk_config['price'],
            "answer" =>$mk_config['answer'],
            "login" =>$mk_config['login'],
            "order" =>$mk_config['oid'],
            "sid" =>$mk_config['sid'],
            "desc" =>trim($mk_config['desc']),
            "sign" =>mk_create_sign($mk_config['phone']),
        );
        
        $post = http_build_query($send);

        $result = mk_send_data($post, $mk_config['api1']);
        if ($result == 'error') {
            $result = mk_send_data($post, $mk_config['api2']);
        }
        if ($result == 'error') {
            return "0";
        } else {
            $result = mk_parse_result($result);
            return $result['status_desc'];
            
            /*if (intval($result['id'])>0) {
                
            } else {
                return $result['status_desc'];
            }*/
        }
    }
}

# Формируем подпись
if(!function_exists("mk_create_sign")) {
    function mk_create_sign($phone = "") {
        global $mk_config;
        return md5($mk_config['login'] . $mk_config['key'] . $mk_config['sid'] . $phone);
    }
}

# Разбираем ответ от скрипта
if(!function_exists("mk_parse_result")) {
    function mk_parse_result($result) {
        $XML = trim($result);
        $returnVal = $XML;
        $emptyTag = '<(.*)/>';
        $fullTag = '<\\1></\\1>';
        $XML = preg_replace("|$emptyTag|", $fullTag, $XML);
        $matches = array();
        if (preg_match_all('|<(.*)>(.*)</\\1>|Ums', trim($XML), $matches)) {
            if (count($matches[1]) > 0)
                $returnVal = array();
            foreach ($matches[1] as $index => $outerXML) {
                $attribute = $outerXML;
                $value = mk_parse_result($matches[2][$index]);
                if (!isset($returnVal[$attribute]))
                    $returnVal[$attribute] = array();
                $returnVal[$attribute][] = $value;
            }
        }
        if (is_array($returnVal))
            foreach ($returnVal as $key => $value) {
                if (is_array($value) && count($value) == 1 && key($value) === 0) {
                    $returnVal[$key] = $returnVal[$key][0];
                }
            }
        return $returnVal;
    }
}

# Отпраляем POST запрос
if(!function_exists("mk_send_data")) {
    function mk_send_data($post, $url) {
        print_r($post);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); # Разрешить переадресацию
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 4); # Таймаут не менять! 
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        $result = curl_exec($ch);
        $status = curl_errno($ch);
        //print_r($post);
        curl_close($ch);
        if ($status == 0 && !empty($result)) {
            return $result;
        } else {
            return "error";
        }
    }
}