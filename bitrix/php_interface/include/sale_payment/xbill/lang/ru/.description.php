<?php
global $MESS;
$MESS["MERCHANT_URL"] = "URL Мерчанта";
$MESS["MERCHANT_URL_DESCR"] = '<b>Режим - API</b>';
$MESS["MERCHANT_URL2"] = "Запасной URL Мерчанта";
$MESS["MERCHANT_URL2_DESCR"] = '<b>Режим - API</b>';

$MESS["MERCHANT_URL_FORM"] = "URL обработчика формы";
$MESS["MERCHANT_URL_FORM_DESCR"] = "<b>Режим - Интерфейс</b>";

$MESS["MERCHANT_ID"] = "Идентификатор проекта";
$MESS["MERCHANT_ID_DESCR"] = 'Узнать его можно в <a href="http://panel.x-bill.org/index.php?page=service">аккаунте X-bill</a><br/>"Аккаунт -> Проекты -> Редактировать".';
$MESS["MERCHANT_LOGIN"] = 'Ваш логин от <a target="_blank" href="http://panel.x-bill.org/">личного кабинета</a> x-bill.org';
$MESS["MERCHANT_LOGIN_DESCR"] = '';
$MESS["SECRET_KEY"] = "Секретный ключ";
$MESS["SECRET_KEY_DESCR"] = 'Должен совпадать с секретным ключем, указанным в <a target="_blank" href="http://panel.x-bill.org/index.php?page=service">аккаунте X-bill</a>:<br/>"Аккаунт -> Проекты -> Редактировать".';

$MESS["ORDER_DESCRIPTION"] = "Описание заказа";
$MESS["ORDER_DESCRIPTION_DESCR"] = "Дополнительный комментарий, отображаемый при оплате.<br>#ID# Будет заменен на номер заказа.<br/><b>Минимум 10 символов</b> максимум 100 символов</i>";
$MESS["ORDER_DESCRIPTION_VALUE"] = "Оплата заказа №#ID#";

$MESS["XBILL_TEST"] = "Тестовый режим";
$MESS["XBILL_YES"] = "Да";
$MESS["XBILL_NO"] = "Нет";
$MESS["XBILL_TEST_DESCR"] = 'Режим отладки, платежи в системе x-bill не засчитываются. (пока не работает)';


$MESS["EMAILERR"] = "Email для ошибок";
$MESS["EMAILERR_DESCR"] = 'Email для отправки ошибок оплаты';
$MESS["ORDER_ID"] = "Номер заказа";
$MESS["SHOULD_PAY"] = "Сумма к оплате";

$MESS["XBILL_MAIN_TITLE"] = "X-bill";
$MESS["XBILL_MAIN_DESCR"] = "<div class='adm-info-message'>
	Платежный сервис <a href='http://x-bill.org' target='_blank'>X-bill.org</a> [<i>Megafon / MTS / Beeline / Smarts / Tele2 / USI</i>]
<p>URL обработчика:<br/>
<input type='text' readonly value='#RESULT_URL#' size='90' /><br/><i>Настройте <strong>bitrix:sale.order.payment.receive</strong> на эту платежную систему.</i></p>

<p>Стандартный URL страницы после успешной оплаты, настраивается в <a target='_blank' href='http://panel.x-bill.org/index.php?page=service'>панели управления</a> [URL OK]:<br/>
<input type='text' readonly value='#OK_URL#' size='90' /><br/><i>Только для режима Интерфейс</i></p>

<p>Стандартный URL страницы неудачной оплаты, настраивается в <a target='_blank' href='http://panel.x-bill.org/index.php?page=service'>панели управления</a> [URL FAIL]:<br/>
<input type='text' readonly value='#ERR_URL#' size='90' /><br/><i>Только для режима Интерфейс</i></p>
</div>";

$MESS["CURL_OK"] = "<span style='color:green;'>установлена</span>";
$MESS["CURL_ERR"] = "<span style='color:red;'>не установлена</span>";
$MESS["ORDER_PHONE_NUMBER"] = "Телефон, на который будет выставлен счет";
$MESS["ORDER_PHONE_NUMBER_DESCR"] = "<b>Только для режима API</b><br><i>К сожалению, в режиме интерфейса номер телефона не может быть автоматически заполнен</i>";

$MESS["ORDER_SMS_ANSWER"] = "Сообщение по СМС клиенту после <b>успешной</b> оплаты";
$MESS["ORDER_SMS_ANSWER_DESCR"] = "<b>Только для режима API</b>, в режим Интерфейс этот текст можно настроить в <a target='_blank' href='http://panel.x-bill.org/index.php?page=service'>панели управления</a><br><b>Внимание! Текст согласовывается отдельно через скайп <a href='skype:sms-stil?chat' target='_blank'/>sms-stil</a></b><br><b>Минимум 10 символов</b> максимум 70 символов";

$MESS["XBILL_TYPE"] = "Режим работы обработчика";
$MESS["XBILL_TYPE_DESCR"] = "<p><b>API</b> - прозрачное выставление счета, покупатель остается на сайте<br/>Необходимая библиотека <a href='http://php.net/manual/ru/curl.installation.php' target='_blank'>cURL</a> #CURL#</p></p><p><b>Интерфейс</b> - редирект покупателя на сайт x-bill.org</p>";
$MESS["XBILL_TYPE_API"] = "API";
$MESS["XBILL_TYPE_FORM"] = "Интерфейс";

