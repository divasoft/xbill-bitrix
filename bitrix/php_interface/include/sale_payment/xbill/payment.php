<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 */

use Bitrix\Main\IO\Path; 
require_once(Path::combine(__DIR__, "functions.php"));

use \Bitrix\Main\Localization\Loc; Loc::loadMessages(__FILE__);

global $mk_config;

$mk_config = array();

$mk_config['type'] = CSalePaySystemAction::GetParamValue("TYPE");

$mk_config['login'] = CSalePaySystemAction::GetParamValue("MERCHANT_LOGIN"); 	# Ваш логин на сайте x-bill.org
$mk_config['key'] = CSalePaySystemAction::GetParamValue("SECRET_KEY"); 	# Контрольная строка (указана в настройках проекта)
$mk_config['sid'] = CSalePaySystemAction::GetParamValue("MERCHANT_ID"); 	# ID проекта

$mk_config['api1'] = CSalePaySystemAction::GetParamValue("MERCHANT_URL", "http://api.x-bill.org/payment.php"); # Основной адрес.
$mk_config['api2'] = CSalePaySystemAction::GetParamValue("MERCHANT_URL2", "http://api.x-bill.ru/payment.php"); # Дополнительный адрес на случай если первый не отвечает.
$mk_config['form'] = CSalePaySystemAction::GetParamValue("MERCHANT_URL_FORM", "http://x-bill.org"); # Адрес обработчика формы, в режиме Интерфейс

$mk_config['oid'] = CSalePaySystemAction::GetParamValue("ORDER_ID");
$mk_config['price'] = number_format(CSalePaySystemAction::GetParamValue("SHOULD_PAY"), 2, '.', '');
$mk_config['phone'] = mk_get_phone(CSalePaySystemAction::GetParamValue("ORDER_PHONE_NUMBER"));
$mk_config['desc'] = CSalePaySystemAction::GetParamValue("ORDER_DESCRIPTION");
$mk_config['desc'] = str_replace("#ID#", $mk_config['oid'], $mk_config['desc']);
$mk_config['answer'] = CSalePaySystemAction::GetParamValue("ORDER_SMS_ANSWER");

//$mk_config['test'] = CSalePaySystemAction::GetParamValue("XBILL_TEST");

$mk_config['sign'] = mk_create_sign($mk_config['phone']);

if ($mk_config['type']=="API") {
    include(dirname(__FILE__).'/tmpl/api.php');
} else {
    include(dirname(__FILE__).'/tmpl/form.php');
}