<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 * 
 * Библиотека "Обработчик"
 * http://x-bill.org/download/script.zip
 */

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
use Bitrix\Main\IO\Path; require_once(Path::combine(__DIR__, "functions.php"));
include(GetLangFileName(dirname(__FILE__) . "/", "/lang.php"));

if (isset($_GET['order']) && isset($_GET['phone']) && isset($_GET['order_status']) && isset($_GET['merchant_price']) && isset($_GET['paytouser']) && isset($_GET['time']) && isset($_GET['sign'])) {
    $secret_key = CSalePaySystemAction::GetParamValue("SECRET_KEY"); # Секретный ключ (Указан в настройках проекта)

    $order = $_GET['order']; # Номер заказа переданный Вами в форму оплаты.
    $phone = $_GET['phone']; # Номер телефона покупателя
    $order_status = $_GET['order_status']; # Статус платежа
    $merchant_price = $_GET['merchant_price']; # Сумма которую оператор списывает со счета покупателя
    //$paytouser = $_GET['paytouser']; # Ваш доход в РУБ.
    $time = date("d.m.Y H:i:s", $_GET['time']); # Дата платежа (Формат:  01.12.2013 22:57:49)
    $sign = $_GET['sign']; # Контрольная подпись

    $truesign = md5($order . $phone . $merchant_price . $secret_key);

    if (!($arOrder = CSaleOrder::GetByID(intval($order)))) {
        exit($arOrder["ID"] . "|error");
    }

    CSalePaySystemAction::InitParamArrays($arOrder, $arOrder["ID"]);

    if ($sign == $truesign) {
        # Если контрольная строка совпадает:
        if ($arOrder["PAYED"] == "N") {
            $arFields = array(
                "PS_STATUS" => "Y",
                "PS_STATUS_CODE" => "-",
                "PS_STATUS_DESCRIPTION" => $order,
                "PS_STATUS_MESSAGE" => $order_status,
                "PS_SUM" => $merchant_price,
                "PS_CURRENCY" => "RUB",
                "PS_RESPONSE_DATE" => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG))),
                "USER_ID" => $arOrder["USER_ID"],
            );

            if (CSaleOrder::Update($arOrder["ID"], $arFields)) {
                if ($arOrder["PRICE"] == $merchant_price) {
                    CSaleOrder::PayOrder($arOrder["ID"], "Y", false);
                    CSaleOrder::StatusOrder($arOrder["ID"], "F");
                    exit("ok");
                } else {
                    exit($arOrder["ID"] . "|error");
                }
            }
        } else {
            exit($arOrder["ID"] . "|error");
        }
    } else {
        # Если контрольная строка не совпадает:
        exit("error(sign)");
    }
} else {
    exit("error(count(" . count($_GET) . "))");
}