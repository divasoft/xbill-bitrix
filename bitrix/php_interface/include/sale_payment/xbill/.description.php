<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Разработка модуля - Divasoft, inc.
 * http://divasoft.ru
 * Версия 1.0
 * 2016
 */

use \Bitrix\Main\Localization\Loc; Loc::loadMessages(__FILE__);

$psTitle = Loc::getMessage("XBILL_MAIN_TITLE");
$psDescription = str_replace("#RESULT_URL#", "http://".$_SERVER['SERVER_NAME']."/payment/xbill/st.php", Loc::getMessage("XBILL_MAIN_DESCR"));
$psDescription = str_replace("#OK_URL#", "http://".$_SERVER['SERVER_NAME']."/payment/xbill/success.php", $psDescription);
$psDescription = str_replace("#ERR_URL#", "http://".$_SERVER['SERVER_NAME']."/payment/xbill/fail.php", $psDescription);

$psTypeDescr=Loc::getMessage("XBILL_TYPE_DESCR");
$curlNeed=Loc::getMessage("CURL_ERR");
if (function_exists('curl_init')) {
    $curlNeed=Loc::getMessage("CURL_OK");
}
$psTypeDescr = str_replace("#CURL#", $curlNeed, $psTypeDescr);


$arPSCorrespondence = array(
        "TYPE" => array(
                "NAME" => Loc::getMessage("XBILL_TYPE"),
                "DESCR" => $psTypeDescr,
                "VALUE" => array(
                        'API' => array('NAME' => Loc::getMessage("XBILL_TYPE_API")),
                        'FORM' => array('NAME' => Loc::getMessage("XBILL_TYPE_FORM"))
                ),
                "TYPE" => "SELECT"
        ),
	"MERCHANT_LOGIN" => array(
		"NAME" => Loc::getMessage("MERCHANT_LOGIN"),
		"DESCR" => Loc::getMessage("MERCHANT_LOGIN_DESCR"),
		"VALUE" => "",
		"TYPE" => ""
	),
	"MERCHANT_ID" => array(
		"NAME" => Loc::getMessage("MERCHANT_ID"),
		"DESCR" => Loc::getMessage("MERCHANT_ID_DESCR"),
		"VALUE" => "",
		"TYPE" => ""
	),
	"SECRET_KEY" => array(
		"NAME" => Loc::getMessage("SECRET_KEY"),
		"DESCR" => Loc::getMessage("SECRET_KEY_DESCR"),
		"VALUE" => "",
		"TYPE" => ""
	),

	"ORDER_PHONE_NUMBER" => array(
		"NAME" => Loc::getMessage("ORDER_PHONE_NUMBER"),
		"DESCR" => Loc::getMessage("ORDER_PHONE_NUMBER_DESCR"),
		"VALUE" => "",
		"TYPE" => "ORDER"
	),

	"ORDER_ID" => array(
		"NAME" => Loc::getMessage("ORDER_ID"),
		"DESCR" => "",
		"VALUE" => "ID",
		"TYPE" => "ORDER"
	),
    
	"SHOULD_PAY" => array(
		"NAME" => Loc::getMessage("SHOULD_PAY"),
		"DESCR" => "",
		"VALUE" => "SHOULD_PAY",
		"TYPE" => "ORDER"
	),
	"ORDER_DESCRIPTION" => array(
		"NAME" => Loc::getMessage("ORDER_DESCRIPTION"),
		"DESCR" => Loc::getMessage("ORDER_DESCRIPTION_DESCR"),
		"VALUE" => Loc::getMessage("ORDER_DESCRIPTION_VALUE"),
		"TYPE" => ""
	),
	"ORDER_SMS_ANSWER" => array(
		"NAME" => Loc::getMessage("ORDER_SMS_ANSWER"),
		"DESCR" => Loc::getMessage("ORDER_SMS_ANSWER_DESCR"),
		"VALUE" => "",
		"TYPE" => ""
	),
        "MERCHANT_URL" => array(
		"NAME" => Loc::getMessage("MERCHANT_URL"),
		"DESCR" => Loc::getMessage("MERCHANT_URL_DESCR"),
		"VALUE" => "http://api.x-bill.org/payment.php",
		"TYPE" => ""
	),
        "MERCHANT_URL2" => array(
		"NAME" => Loc::getMessage("MERCHANT_URL2"),
		"DESCR" => Loc::getMessage("MERCHANT_URL2_DESCR"),
		"VALUE" => "http://api.x-bill.ru/payment.php",
		"TYPE" => ""
	),
        "MERCHANT_URL_FORM" => array(
		"NAME" => Loc::getMessage("MERCHANT_URL_FORM"),
		"DESCR" => Loc::getMessage("MERCHANT_URL_FORM_DESCR"),
		"VALUE" => "http://x-bill.org",
		"TYPE" => ""
	),

	/*"EMAILERR" => array(
		"NAME" => Loc::getMessage("EMAILERR"),
		"DESCR" => Loc::getMessage("EMAILERR_DESCR"),
		"VALUE" => "",
		"TYPE" => ""
	),*/
        /*"XBILL_TEST" => array(
                "NAME" => Loc::getMessage("XBILL_TEST"),
                "DESCR" => Loc::getMessage("XBILL_TEST_DESCR"),
                "VALUE" => array(
                        'Y' => array('NAME' => Loc::getMessage("XBILL_YES")),
                        'N' => array('NAME' => Loc::getMessage("XBILL_NO"))
                ),
                "TYPE" => "SELECT"
        ),*/
);